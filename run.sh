#!/usr/bin/env bash
docker run -p 3306:3306 --rm --name mysql \
  -e MYSQL_ROOT_HOST='%' -e MYSQL_ROOT_USERNAME=root -e MYSQL_ROOT_PASSWORD=pass \
  -v "$(pwd)/data:/var/lib/mysql" mysql:5.7 \
  mysqld --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci